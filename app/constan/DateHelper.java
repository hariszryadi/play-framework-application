package constan;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

    public String convertDate(Date date){

        SimpleDateFormat dt1 = new SimpleDateFormat("yyyy-MM-dd");
        return dt1.format(date);
    }

    public String currencySubTotal(String sub_total) {
        double m = Double.parseDouble(sub_total);
        DecimalFormat formatter = new DecimalFormat("Rp ###,###,###");
        return formatter.format(m);
    }

    public String currencyPrice(String price) {
        double m = Double.parseDouble(price);
        DecimalFormat formatter = new DecimalFormat("Rp ###,###,###");
        return formatter.format(m);
    }
}
